import json
from app.gateways.kafka import Kafka
from fastapi import APIRouter, Depends
from typing import Optional
from pydantic import BaseModel


def get_kafka_instance():
    if Kafka.instance:
        return Kafka.instance
    return Kafka()


#Валидация данных через pydantic
class Message(BaseModel):
    name: str
    text: Optional[str] = None


router = APIRouter()

@router.post("")
async def send(data: Message, server: Kafka = Depends(get_kafka_instance)):
    try:
        topic_name = server._topic
        await server.aioproducer.send_and_wait(topic_name,
                                               json.dumps(data.dict()).encode("ascii"))
    except Exception as e:
        await server.aioproducer.stop()
        raise e
    return 'Сообщение отправлено'
