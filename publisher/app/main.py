import time
import os
from app.routers import publisher
from app.gateways.kafka import Kafka
from dotenv import load_dotenv
from fastapi import Depends, FastAPI, Request
from enum import Enum

class EnvironmentVariables(str, Enum):
    KAFKA_TOPIC_NAME = 'KAFKA_TOPIC_NAME'
    KAFKA_SERVER = 'KAFKA_SERVER'
    KAFKA_PORT = 'KAFKA_PORT'

    def get_env(self, variable=None):
        return os.environ.get(self, variable)


def get_kafka_instance():
    if Kafka.instance:
        return Kafka.instance
    return Kafka()



load_dotenv()
app = FastAPI(title='Kafka Publisher API')
kafka_server = Kafka(
    topic=EnvironmentVariables.KAFKA_TOPIC_NAME.get_env(),
    port=EnvironmentVariables.KAFKA_PORT.get_env(),
    servers=EnvironmentVariables.KAFKA_SERVER.get_env(),
)

#Перед стартом
@app.on_event("startup")
async def startup_event():
    await kafka_server.aioproducer.start()

#После останови приложения
@app.on_event("shutdown")
async def shutdown_event():
    await kafka_server.aioproducer.stop()

#Перед каждым request
@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


@app.get('/')
def get_root():
    return {'message': 'API is running...'}


app.include_router(
    publisher.router,
    prefix="/producer",
    tags=["producer"],
    dependencies=[Depends(get_kafka_instance)],
)
