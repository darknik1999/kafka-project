# kafka-project


## docker-контейнеры

* zookeeper
* kafka - брокер
* publisher - фреймворк FastAPI, запросы по топика `first_topic` передаются на http://localhost:8000/producer, GET запрос о состоянии publisher по http://localhost:8000/
* kafdrop - kafka UI.
* consumer - принимает сообщение из партиции и пишет логи

## Запускаем контейнеры

```
docker-compose up -d --build
#Далем POST запрос на publisher
curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "nick", "text": "YUMP"}' http://localhost:8000/producer
#Смотрим логи в контейнере consumer
docker compose logs consumer

```
## Kafkdrop запущен на http://localhost:19000
Можно посмотреть на состояние kafka
